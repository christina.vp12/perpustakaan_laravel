<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";
    public function book(){
    	return $this->hasMany('App\book');
    }
}
