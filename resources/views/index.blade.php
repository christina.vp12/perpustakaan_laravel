<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>

	<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

	<h3>Data Buku</h3>

	<a href="/book/tambah" class="btn btn-primary btn-sm"> + Tambah Buku</a>
	<a href="/book/kategori" class="btn btn-primary btn-sm"> Kategori Buku</a>
	<table class="table table-hover">
		<tr>
			<th>Judul</th>
			<th>Penulis</th>
			<th>Penerbit</th>
			<th>Kategori</th>
		</tr>
		@foreach($book as $b)
		<tr>
			<td>{{ $b->judul }}</td>
			<td>{{ $b->penulis }}</td>
			<td>{{ $b->penerbit }}</td>
			<td>{{ $b->kategori }}</td>
			<td>
				<a href="/book/edit/{{ $b->id }}" class="btn btn-info btn-sm">Edit</a>
				|
				<a onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')" href="/book/hapus/{{ $b->id }}" class="btn btn-danger btn-sm">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>

	<br/>

	{{ $book->links() }}


</body>
</html>