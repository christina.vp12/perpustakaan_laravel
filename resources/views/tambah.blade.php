<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
 
	<h3>Data Buku</h3>
 
	<a href="/book"> Kembali</a>
	
	<br/>
 
	<form action="/book/store" method="post">
		{{ csrf_field() }}
		Judul <input type="text" clas="form-control class @error('judul') is-invalid @enderror" name="judul" >
		@error('judul')
		<class='invalid-feedback'>{{$message}}
		 <br/>@enderror<br>

		Penulis <input type="text" clas="form-control class @error('penulis') is-invalid @enderror" name="penulis"> 
		@error('penulis')<br>
		<class='invalid-feedback'>{{$message}}
		 <br/>@enderror<br>

		Penerbit <input type="text" clas="form-control class @error('penerbit') is-invalid @enderror" name="penerbit">
		@error('Penerbit')<br>
		<class='invalid-feedback'> {{$message}}<br/>@enderror<br>
		
		Kategori <select name="kategori"  >
				<option value="Komik">Komik</option>
				<option value="Antologi">Antologi</option>
				<option value="Biografi">Biografi</option>
				</select>
		<br/>
		<input type="submit" value="Simpan Data">
	</form>
 
</body>
</html>