<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
	<h3>Edit Buku</h3>

	<a href="/"> Kembali</a>
	
	<br/>
	<br/>

	@foreach($book as $b)
	<form action="/book/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $b->id }}"> <br/>
		Judul <input type="text" clas="form-control class @error('judul') is-invalid @enderror" name="judul" value="{{ $b->judul }}" >
		@error('judul')
		<class='invalid-feedback'>{{$message}}
		 <br/>@enderror<br>
		Penulis <input type="text" clas="form-control class @error('penulis') is-invalid @enderror" name="penulis" value="{{ $b->penulis }}"> 
		@error('penulis')<br>
		<class='invalid-feedback'>{{$message}}
		 <br/>@enderror<br>
		Penerbit <input type="text" clas="form-control class @error('penerbit') is-invalid @enderror" name="penerbit" value="{{ $b->penerbit }}">
		@error('Penerbit')<br>
		<class='invalid-feedback'> {{$message}}<br/>@enderror<br>
		Kategori <select name="kategori" value="{{ $b->kategori }}"  >
				<option value="Komik">Komik</option>
				<option value="Antologi">Antologi</option>
				<option value="Biografi">Biografi</option>
				</select>
		<br/>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach		

</body>
</html>